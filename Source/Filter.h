/*
  ==============================================================================

    Filter.h
    Created: 15 May 2020 4:34:50pm
    Author:  dell

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>
class Filter {
public:
    enum FilterMode {
        FILTER_MODE_LOWPASS = 0,
        FILTER_MODE_HIGHPASS,
        FILTER_MODE_BANDPASS,
        NULL1
    };
    Filter() :
        cutoff(0.99),
        resonance(0.0),
        mode(FILTER_MODE_LOWPASS),
        buf0(0.0),
        buf1(0.0)
    {
        calculateFeedbackAmount();
    };
    double process(double inputValue);
    inline void setCutoff(double newCutoff) { cutoff = newCutoff; calculateFeedbackAmount(); };
    inline void setResonance(double newResonance) { resonance = newResonance; calculateFeedbackAmount(); };
    inline void setFilterMode(double newMode) 
    { 
        if (newMode == 0.0) mode = FILTER_MODE_LOWPASS; 
        if (newMode == 1.0) mode = FILTER_MODE_HIGHPASS;
        if (newMode == 2.0) mode = FILTER_MODE_BANDPASS;
        if (newMode == 3.0) mode = NULL1;
    }
private:
    double cutoff;
    double resonance;
    FilterMode mode;
    double feedbackAmount;
    inline void calculateFeedbackAmount() { feedbackAmount = resonance + resonance / (1.0 - cutoff); }
    double buf0;
    double buf1;
};