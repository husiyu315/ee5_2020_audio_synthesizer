/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
//==============================================================================
/**
*/
class Ee5AudioProcessorEditor  : public AudioProcessorEditor,
                                 private Slider::Listener
{
public:
    Ee5AudioProcessorEditor (Ee5AudioProcessor&);
    ~Ee5AudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:
    void sliderValueChanged(Slider* slider) override; // [3]
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    Ee5AudioProcessor& processor;
    Slider midiVolume;
    Slider WaveShape;
    Slider WaveShape2;
    Slider A;
    Slider D;
    Slider S;
    Slider R;
    Slider noiseS;
    Slider mix1;
    Slider mix2;
    Slider cutoff;
    Slider resonance;
    Slider filtType;
    Label waveLabel;
    Label waveLabel2;
    Label volumeLable;
    Label Attack;
    Label Decay;
    Label Sustain;
    Label Release;
    Label noiseLabel;
    Label mix1Label;
    Label mix2Label;
    Label cutoffLabel;
    Label resonanceLabel;
    Label filtLabel;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Ee5AudioProcessorEditor)
};
