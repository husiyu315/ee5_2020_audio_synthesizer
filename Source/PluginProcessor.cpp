/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
Ee5AudioProcessor::Ee5AudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
}

Ee5AudioProcessor::~Ee5AudioProcessor()
{
}

//==============================================================================
const String Ee5AudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool Ee5AudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool Ee5AudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool Ee5AudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double Ee5AudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int Ee5AudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int Ee5AudioProcessor::getCurrentProgram()
{
    return 0;
}

void Ee5AudioProcessor::setCurrentProgram (int index)
{
}

const String Ee5AudioProcessor::getProgramName (int index)
{
    return {};
}

void Ee5AudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void Ee5AudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    wtSize = 1024;		
    freq = 441;		
    phase = 0;		
    amp = 0.1;		
    increment = (freq /sampleRate) * wtSize;
    for (int i = 0; i < wtSize; ++i)
    {
        sinWT.insert(i, sin(2 * float_Pi * i / wtSize));
    }
    currentsamplerate = sampleRate;
}

void Ee5AudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool Ee5AudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void Ee5AudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    buffer.clear();
    MidiBuffer processedMidi; 
    AudioSourceChannelInfo bufferToFill;
    bufferToFill.buffer = &buffer;
    int time;
    MidiMessage m;
    float spr;
    spr = getSampleRate();
    for (MidiBuffer::Iterator i(midiMessages); i.getNextEvent(m, time);)
    {
        if (m.isNoteOn())
        {
            uint8 newVel = (uint8)noteOnVel;
            m = MidiMessage::noteOn(m.getChannel(), m.getNoteNumber(), newVel);  
            note = m.getNoteNumber();
            trigger = 1;
        }
        else if (m.isNoteOff())
        {
            trigger = 0;//release envelop
        }
        else if (m.isAftertouch())
        {
        }
       else if (m.isPitchWheel())
        {
        }
        processedMidi.addEvent(m, time);
    }
    midiMessages.swapWith(processedMidi);
    
    getNextAudioBlock(buffer,0,trigger);

}
void Ee5AudioProcessor::update(int midiNoteNumber)
{
    freq = MidiMessage::getMidiNoteInHertz(midiNoteNumber);		
    amp = 0.1;	 
    filt.setCutoff(sin(2*PI*freq/samplingRate)*cut_off);
    //filt.setCutoff(cut_off);
    filt.setResonance(resonance_f);
    filt.setFilterMode(filterT);	
}

void Ee5AudioProcessor::getNextAudioBlock(AudioBuffer<float>& buffer, int startSample, int trigger)
{
    for (auto sample = 0;sample < buffer.getNumSamples();++sample)
    {   
        adsrtest(A1,D1,S1,R1,trigger); 
        auto myWave = osc1.square(freq)*0.1*(waveSelect==0)+osc1.triangle(freq) * 0.1* (waveSelect == 1)+
            osc1.saw(freq) * 0.1* (waveSelect == 2) +osc1.sinewave(freq) * 0.1* (waveSelect == 3);
        auto noise = osc3.noise();
        auto myWave2= osc2.square(freq) * 0.1 * (waveSelect1 == 0) + osc2.triangle(freq) * 0.1 * (waveSelect1 == 1) +
            osc2.saw(freq) * 0.1 * (waveSelect1 == 2) + osc2.sinewave(freq) * 0.1 * (waveSelect1 == 3);
        totalWave = (myWave*mix1V + myWave2*mix2V + noise*0.1*noiseV) ;
        
        for (auto channel = 0; channel < buffer.getNumChannels(); ++channel)
        {
            buffer.addSample(channel, startSample, filt.process(totalWave) * amplitude * 0.1 * noteOnVel);
        }
        ++startSample;
    }
    update(note);
}

void Ee5AudioProcessor::adsrtest(double attack, double decay, double sustain, double release, int trigger)
{
    if (trigger == 1 && attackphase != 1 && holdphase != 1 && decayphase != 1) {
        holdcount = 0;
        decayphase = 0;
        sustainphase = 0;
        releasephase = 0;
        attackphase = 1;
        amplitude = 0;
    }

    if (attackphase == 1) {
        releasephase = 0;
        amplitude = amplitude + attack*0.001;
        //amplitude = 1;
        if (amplitude >= 1) {
            amplitude = 1;
            attackphase = 0;
            decayphase = 1;
        }
    }
    if (decayphase == 1) {
        amplitude = amplitude * decay;
        if (amplitude <= sustain) {
            decayphase = 0;
            holdphase = 1;
        }
    }
    if (holdphase == 1)
    {
        amplitude = amplitude;
    }
    if ( trigger != 1) {
        holdphase = 0;
        releasephase = 1;
    }

    if (releasephase == 1 && amplitude > 0) {
        amplitude = amplitude * release;
    }
}


//==============================================================================
bool Ee5AudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* Ee5AudioProcessor::createEditor()
{
    return new Ee5AudioProcessorEditor (*this);
}

//==============================================================================
void Ee5AudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void Ee5AudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new Ee5AudioProcessor();
}