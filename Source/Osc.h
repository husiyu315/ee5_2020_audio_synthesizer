/*
  ==============================================================================

    Osc.h
    Created: 29 Apr 2020 6:28:25pm
    Author:  dell

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>
#ifndef PI
#define PI  3.1415926535897932384626433832795
#endif
#define TWOPI 6.283185307179586476925286766559
#define samplingRate 44100
class Osc {
	double frequency;
	double phase;
	double phase1;
	double phase2;
	double phase3;
	double startphase;
	double endphase;
	double output;
	double tri;
public:
	Osc();
	double sinewave(double frequency);
	double saw(double frequency);
	double triangle(double frequency);
	double square(double frequency);
	double noise();
};
