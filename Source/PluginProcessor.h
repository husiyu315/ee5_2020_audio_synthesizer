/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "math.h"
#include <iterator>
#include "Osc.h"
#include "Filter.h"
//==============================================================================
/**
*/
class Ee5AudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    Ee5AudioProcessor();
    ~Ee5AudioProcessor();
    float noteOnVel;
    float noiseV;
    float mix1V;
    float mix2V;
    //maxiOsc osc;//oscillator
    Osc osc1;
    Osc osc2;
    Osc osc3;//noise oscilator
    IIRFilter filta;
    Filter filt;
    double myWave;
    double myWave2;
    double noise;
    double totalWave;
    double testWave;
    int trigger;
    double waveSelect;
    double waveSelect1;
    double cut_off;
    double filterT;
    double resonance_f;
    //===================================ENVELOP=======================================
    int attackphase, decayphase, sustainphase, holdphase, releasephase;
    long holdcount;
    double amplitude;
    double output;
    double A1;
    double D1;
    double S1;
    double R1;
    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioBuffer<float>&, MidiBuffer&) override;
    void update(int midiNoteNumber);
    void getNextAudioBlock(AudioBuffer<float>&,int startSample, int trigger);
    void adsrtest(double attack, double decay, double sustain, double release, int trigger);
    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

private:
    //==============================================================================
    Array<float> sinWT;		//
    float wtSize;			//
    float phase;			//
    float increment;		//
    float freq;			//
    float amp;			//
    double currentsamplerate;
    int note;
   
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Ee5AudioProcessor)
};
