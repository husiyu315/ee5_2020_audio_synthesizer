/*
  ==============================================================================

    Osc.cpp
    Created: 29 Apr 2020 6:28:25pm
    Author:  dell

  ==============================================================================
*/

#include "Osc.h"
Osc::Osc() {
    phase = 0;
    phase1 = 0;
    phase2 = 0;
    phase3 = 0;
}

double Osc::sinewave(double frequency)
{
    output = sin(phase * (TWOPI));
    if (phase >= 1.0) phase -= 1.0;
    phase += (1. / (samplingRate / (frequency)));
    return(output);
}

double Osc::saw(double frequency)
{
    output = phase1;
    if (phase1 >= 1.0) phase1 -= 2.0;
    phase1 += (1. / (samplingRate / (frequency))) * 2.0;
    return(output);
}

double Osc::triangle(double frequency)
{
    //This is a triangle wave.
    if (phase2 >= 1.0) phase2 -= 1.0;
    phase2 += (1. / (samplingRate / (frequency)));
    if (phase2 <= 0.5) {
        output = (phase2 - 0.25) * 4;
    }
    else {
        output = ((1.0 - phase2) - 0.25) * 4;
    }
    return(output);
}

double Osc::square(double frequency)
{
    //This is a square wave
    if (phase3 < 0.5) output = -1;
    if (phase3 > 0.5) output = 1;
    if (phase3 >= 1.0) phase3 -= 1.0;
    phase3 += (1. / (samplingRate / (frequency)));
    return(output);
}

double Osc::noise()
{
    //White Noise
    //always the same unless seed it.
    float r = rand() / (float)RAND_MAX;
    output = r * 2 - 1;
    return(output);
}

