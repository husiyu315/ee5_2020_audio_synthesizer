/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
//==============================================================================
Ee5AudioProcessorEditor::Ee5AudioProcessorEditor (Ee5AudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (800, 600);
    // these define the parameters of our slider object
    midiVolume.setSliderStyle(Slider::Rotary);
    midiVolume.setRange(0.0, 127.0, 1.0);
    midiVolume.setTextBoxStyle(Slider::TextBoxBelow, false, 90, 30);
    midiVolume.setPopupDisplayEnabled(true, false, this);
    midiVolume.setTextValueSuffix(" Volume");
    midiVolume.setValue(20.0);
    // ==============================Wave=====select=================
    WaveShape.setSliderStyle(Slider::Rotary);
    WaveShape.setRange(0, 3.0, 1.0);
    WaveShape.setTextBoxStyle(Slider::TextBoxBelow, false, 90, 30);
    WaveShape.setPopupDisplayEnabled(true, false, this);
    WaveShape.setValue(3.0);
    // ==============================Wave=====select2=================
    WaveShape2.setSliderStyle(Slider::Rotary);
    WaveShape2.setRange(0, 3.0, 1.0);
    WaveShape2.setTextBoxStyle(Slider::TextBoxBelow, false, 90, 30);
    WaveShape2.setPopupDisplayEnabled(true, false, this);
    WaveShape2.setValue(3.0);
    //================================A========================
    A.setSliderStyle(Slider::Rotary);
    A.setRange(0, 1.0, 0.001);
    A.setTextBoxStyle(Slider::NoTextBox, false, 90, 30);
    A.setPopupDisplayEnabled(true, false, this);
    A.setValue(1.0);
    //================================D========================
    D.setSliderStyle(Slider::Rotary);
    D.setRange(0.5, 1.0, 0.0001);
    D.setTextBoxStyle(Slider::NoTextBox, false, 90, 30);
    D.setPopupDisplayEnabled(true, false, this);
    D.setValue(0.99);
    //================================S========================
    S.setSliderStyle(Slider::Rotary);
    S.setRange(0, 1.0, 0.0001);
    S.setTextBoxStyle(Slider::NoTextBox, false, 90, 30);
    S.setPopupDisplayEnabled(true, false, this);
    S.setValue(0.125);
    //================================R========================
    R.setSliderStyle(Slider::Rotary);
    R.setRange(0.99, 0.99999, 0.00009);
    R.setTextBoxStyle(Slider::NoTextBox, false, 90, 30);
    R.setPopupDisplayEnabled(true, false, this);
    R.setValue(0.99);
    //================================Noise========================
    noiseS.setSliderStyle(Slider::Rotary);
    noiseS.setRange(0.0, 1.0, 0.01);
    noiseS.setTextBoxStyle(Slider::TextBoxBelow, false, 90, 30);
    noiseS.setPopupDisplayEnabled(true, false, this);
    noiseS.setValue(0);
    //================================mix1========================
    mix1.setSliderStyle(Slider::Rotary);
    mix1.setRange(0.0, 1.0, 0.01);
    mix1.setTextBoxStyle(Slider::TextBoxBelow, false, 90, 30);
    mix1.setPopupDisplayEnabled(true, false, this);
    mix1.setValue(1);
    //================================mix2========================
    mix2.setSliderStyle(Slider::Rotary);
    mix2.setRange(0.0, 1.0, 0.01);
    mix2.setTextBoxStyle(Slider::TextBoxBelow, false, 90, 30);
    mix2.setPopupDisplayEnabled(true, false, this);
    mix2.setValue(0);
    //================================filt_type========================
    filtType.setSliderStyle(Slider::Rotary);
    filtType.setRange(0.0, 3.0, 1.0);
    filtType.setTextBoxStyle(Slider::TextBoxBelow, false, 90, 30);
    filtType.setPopupDisplayEnabled(true, false, this);
    filtType.setValue(0);
    //================================cut off========================
    cutoff.setSliderStyle(Slider::Rotary);
    cutoff.setRange(0.0, 2, 0.01);
    cutoff.setTextBoxStyle(Slider::TextBoxBelow, false, 90, 30);
    cutoff.setPopupDisplayEnabled(true, false, this);
    cutoff.setValue(0.99);
     //================================resonance========================
    resonance.setSliderStyle(Slider::Rotary);
    resonance.setRange(0.01, 1.0, 0.001);
    resonance.setTextBoxStyle(Slider::TextBoxBelow, false, 90, 30);
    resonance.setPopupDisplayEnabled(true, false, this);
    resonance.setValue(0.01);
    // this function adds the slider to the editor
    addAndMakeVisible(&midiVolume);
    volumeLable.setText("Volume", dontSendNotification);
    volumeLable.attachToComponent(&midiVolume, false);
    addAndMakeVisible(&WaveShape);
    waveLabel.setText("Osc1 WaveSelect", dontSendNotification);
    waveLabel.attachToComponent(&WaveShape, false);
    addAndMakeVisible(&WaveShape2);
    waveLabel2.setText("Osc2 WaveSelect", dontSendNotification);
    waveLabel2.attachToComponent(&WaveShape2, false);

    addAndMakeVisible(&A);
    Attack.setText("Attack", dontSendNotification);
    Attack.attachToComponent(&A, false);
    addAndMakeVisible(&D);
    Decay.setText("Decay", dontSendNotification);
    Decay.attachToComponent(&D, false);
    addAndMakeVisible(&S);
    Sustain.setText("Sustain", dontSendNotification);
    Sustain.attachToComponent(&S, false);
    addAndMakeVisible(&R);
    Release.setText("Release", dontSendNotification);
    Release.attachToComponent(&R, false);
    addAndMakeVisible(&noiseS);
    noiseLabel.setText("Noise", dontSendNotification);
    noiseLabel.attachToComponent(&noiseS, false);
    addAndMakeVisible(&mix1);
    mix1Label.setText("Osc1", dontSendNotification);
    mix1Label.attachToComponent(&mix1, false);
    addAndMakeVisible(&mix2);
    mix2Label.setText("Osc2", dontSendNotification);
    mix2Label.attachToComponent(&mix2, false);
    addAndMakeVisible(&filtType);
    filtLabel.setText("filter", dontSendNotification);
    filtLabel.attachToComponent(&filtType, false);
    addAndMakeVisible(&cutoff);
    cutoffLabel.setText("cut_off", dontSendNotification);
    cutoffLabel.attachToComponent(&cutoff, false);
    addAndMakeVisible(&resonance);
    resonanceLabel.setText("Resonance", dontSendNotification);
    resonanceLabel.attachToComponent(&resonance, false);
    // add the listener to the slider
    midiVolume.addListener(this);
    WaveShape.addListener(this);
    WaveShape2.addListener(this);
    A.addListener(this);
    D.addListener(this);
    S.addListener(this);
    R.addListener(this);
    noiseS.addListener(this);
    mix1.addListener(this);
    mix2.addListener(this);
    filtType.addListener(this);
    cutoff.addListener(this);
    resonance.addListener(this);
}

Ee5AudioProcessorEditor::~Ee5AudioProcessorEditor()
{
}
void Ee5AudioProcessorEditor::sliderValueChanged(Slider* slider)
{
    processor.noteOnVel = midiVolume.getValue();
    processor.waveSelect = WaveShape.getValue();
    processor.waveSelect1 = WaveShape2.getValue();
    processor.A1 = A.getValue();
    processor.D1 = D.getValue();
    processor.S1 = S.getValue();
    processor.R1 = R.getValue();
    processor.noiseV = noiseS.getValue();
    processor.mix1V = mix1.getValue();
    processor.mix2V = mix2.getValue();
    processor.cut_off = cutoff.getValue();
    processor.filterT = filtType.getValue();
    processor.resonance_f = resonance.getValue();
    if(WaveShape.getValue() == (double)0.0)WaveShape.setTextValueSuffix("Square");
    if(WaveShape.getValue() == (double)1.0)WaveShape.setTextValueSuffix("Triangle");
    if(WaveShape.getValue() == (double)2.0)WaveShape.setTextValueSuffix("Saw");
    if(WaveShape.getValue() == (double)3.0)WaveShape.setTextValueSuffix("Sine");
    if (WaveShape2.getValue() == (double)0.0)WaveShape2.setTextValueSuffix("Square");
    if (WaveShape2.getValue() == (double)1.0)WaveShape2.setTextValueSuffix("Triangle");
    if (WaveShape2.getValue() == (double)2.0)WaveShape2.setTextValueSuffix("Saw");
    if (WaveShape2.getValue() == (double)3.0)WaveShape2.setTextValueSuffix("Sine");
    if (filtType.getValue() == (double)0.0)filtType.setTextValueSuffix("LP");
    if (filtType.getValue() == (double)1.0)filtType.setTextValueSuffix("HP");
    if (filtType.getValue() == (double)2.0)filtType.setTextValueSuffix("BP");
    if (filtType.getValue() == (double)3.0)filtType.setTextValueSuffix("DEFAULT");
}

//==============================================================================
void Ee5AudioProcessorEditor::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    g.setColour (Colours::violet);
    g.setFont (15.0f);
    g.drawFittedText ("EE5", getLocalBounds(), Justification::bottom, 1);
    g.drawRect(0, 0, 150, 600);
    g.drawRect(150,170, 650, 200);
}

void Ee5AudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..

    // sets the position and size of the slider with arguments (x, y, width, height)
    mix1.setBounds(150, 190, 180, 180);
    mix2.setBounds(350, 190, 180, 180);
    noiseS.setBounds(550, 190, 180, 180);
    WaveShape.setBounds(0, 20, 150, 150);
    WaveShape2.setBounds(0, 190, 150, 150);
    A.setBounds(150, 20, 150, 150);
    D.setBounds(300, 20, 150, 150);
    S.setBounds(450, 20, 150, 150);
    R.setBounds(600, 20, 150, 150);
    midiVolume.setBounds (0, 360, 150, 150);
    filtType.setBounds(150,400,180, 180);
    cutoff.setBounds(350, 400, 180, 180);
    resonance.setBounds(550, 400, 180, 180);
}
